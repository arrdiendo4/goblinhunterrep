﻿#pragma strict

private var myAnimator:Animation;
public var movementModificator:int;
public var rotationModificator:int;
public var fireball:GameObject;	
private var attackTime:float;
	
	
function Start () {
	myAnimator = gameObject.GetComponent(Animation);
	attackTime = 1.0F;
}

function Update () {

}

function FixedUpdate(){
attackTime -= Time.deltaTime;
}

public function envant(){
	Debug.Log("envant");
	transform.Translate(Vector3.forward * movementModificator * Time.deltaTime);
	myAnimator.Play("walk", PlayMode.StopSameLayer);
}


public function enrera(){
	Debug.Log("enrera");
	var newPos:Vector3 = GetBehindPosition();
	transform.LookAt(newPos);
}


public function dreta(){
	Debug.Log("dreta");
	transform.Rotate(new Vector3(0, 1, 0) * rotationModificator);
}


public function esquerra(){
	Debug.Log("esquerra");
	transform.Rotate(new Vector3(0, -1, 0) * rotationModificator);
}

public function attack(){
if(attackTime <= 0){
	var vectorPosition:Vector3 = new Vector3(transform.position.x, transform.position.y + 20, transform.position.z);
	var myFireball:GameObject = Instantiate(fireball, vectorPosition, Quaternion.identity);
	myFireball.GetComponent.<Rigidbody>().AddForce(transform.forward * 1000);
	Debug.Log(transform.forward.ToString());
	attackTime = 1.0F;
	}
}

function GetBehindPosition() {
     return gameObject.transform.position - gameObject.transform.forward;
 }