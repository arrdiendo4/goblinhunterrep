﻿using UnityEngine;
using System.Collections;

public class Rotar : MonoBehaviour {

	private Transform objetoCentroDeRotacion;
	public float rotacionPorSegundo = 75f;
	// Use this for initialization
	void Start () {
		objetoCentroDeRotacion = GameObject.Find ("DustStorm").transform;
	}
	
	// Update is called once per frame
	void Update () {
		transform.RotateAround (objetoCentroDeRotacion.position, Vector3.up, rotacionPorSegundo * Time.deltaTime);
	}
}
