﻿using UnityEngine;
using System.Collections;

public class scoreGUI : MonoBehaviour {

	public int numGoblins;
	public Font fuenteLetra;

	void Start () {
		numGoblins = 0;
	}
	
	void Update () {
		
	}
	
	void OnGUI() {
		GUIStyle StyleLetra = new GUIStyle();
		StyleLetra.normal.textColor = Color.white;
		StyleLetra.font = fuenteLetra;
		StyleLetra.fontSize = 48;
		//Rect rectangulo = new Rect (Screen.width - 50, 10, 100, 50);
		Rect rectangulo = new Rect (Screen.width - Screen.width *0.25f, 10, Screen.width *0.2f, Screen.height *0.2f);
		GUI.Label(rectangulo, numGoblins.ToString(), StyleLetra);
	}
	
	
	public void ReiniciarContadorGoblinsMuertos(){
		numGoblins= 0;
	}}