﻿using UnityEngine;
using System.Collections;

public class goblinBehaviour : MonoBehaviour {

	public GameObject goblinCry;
	private scoreGUI scoreGUIScript;

	// Use this for initialization

	private GameObject baseMale;
	private GameObject ControladorEscena;

	void Start () {
		baseMale = GameObject.Find ("Marcador/baseMale");
		ControladorEscena = GameObject.Find ("ControladorEscena");
		scoreGUIScript = (scoreGUI)baseMale.GetComponent<scoreGUI>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider colission){
		if (colission.gameObject.tag == "fireball"){
			scoreGUIScript.numGoblins++;
			PlayerPrefs.SetInt("Puntuacion", scoreGUIScript.numGoblins);
			Component GeneradorGoblins = ControladorEscena.GetComponent("GeneradorGoblins");
			GeneradorGoblins.SendMessage("decrementarContador");
			Vector3 posGoblin = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y +17, gameObject.transform.position.z);
			Instantiate(goblinCry, posGoblin, Quaternion.identity);
			Destroy(gameObject);
		}
	}
}