﻿using UnityEngine;
using System.Collections;

public class GeneradorGoblins : MonoBehaviour {
	public GameObject goblinPrefab;
	public Transform padreDeNuestrosGoblins;
	public Transform objetoEnElQueGirar;

	public float esperaAlPrimerGoblin = 3f;
	public float tiempoEntreGoblins = 5f;
	static public int ContadorGoblins = 1;

	private scoreGUI scoreGUIScript;

	public Font fuenteLetra;

	private float horaDelSiguienteGoblin;
	void Awake(){

		scoreGUIScript = (scoreGUI)GetComponent<scoreGUI>();
	}

	void Start () {
		horaDelSiguienteGoblin = Time.time + esperaAlPrimerGoblin;
	}

	void Update () {
		if (ContadorGoblins >= 15) {
			if (scoreGUIScript.numGoblins <= 34) {
				GameObject baseMale = GameObject.Find ("baseMale");
				Component scoreGUI = baseMale.GetComponent ("scoreGUI");
				Application.LoadLevel ("gameOverScene");
				reiniciarContadorGoblinsVivos ();
				scoreGUI.SendMessage ("ReiniciarContadorGoblinsMuertos", null);
			} else {
				GameObject baseMale = GameObject.Find ("baseMale");
				Component scoreGUI = baseMale.GetComponent ("scoreGUI");
				Application.LoadLevel ("gameWinnedScene");
				reiniciarContadorGoblinsVivos ();
				scoreGUI.SendMessage ("ReiniciarContadorGoblinsMuertos", null);
			}
		}
		if (Time.time >= horaDelSiguienteGoblin) {
			tiempoEntreGoblins = tiempoEntreGoblins * 0.9f;
			horaDelSiguienteGoblin = Time.time + tiempoEntreGoblins;
			float posicionX = Random.Range (-120.0F, 120.0F);
			float posicionZ = Random.Range (-50.0F, 50.0F);
			Vector3 posicionGoblin = new Vector3 (posicionX, objetoEnElQueGirar.transform.position.y, posicionZ);
			//Transform goblinTransform = Instantiate(goblinPrefab, posicionGoblin , Quaternion.identity) as Transform;
			ContadorGoblins += 1;
			GameObject goblinTransform = Instantiate (goblinPrefab, posicionGoblin, Quaternion.identity) as GameObject;
			goblinTransform.transform.parent = padreDeNuestrosGoblins;
		}
	}
	

	public void decrementarContador(){
		ContadorGoblins -= 1;
	}

	public void reiniciarContadorGoblinsVivos(){
		ContadorGoblins = 1;
	}

	void OnGUI() {
		GUIStyle StyleLetra = new GUIStyle();
		StyleLetra.normal.textColor = Color.white;
		StyleLetra.font = fuenteLetra;
		StyleLetra.fontSize = 48;
		GUI.Label(new Rect(Screen.width *0.1f,10 ,Screen.width *0.2f, Screen.height *0.2f), ContadorGoblins.ToString(),StyleLetra);
	}

	public int puntuacion(){
		int goblinsEliminados = scoreGUIScript.numGoblins;
		return goblinsEliminados;
	}
	

	}
