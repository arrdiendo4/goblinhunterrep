﻿using UnityEngine;
using System.Collections;

public class DestruidorBolas : MonoBehaviour {

	private float TiempoParaDestruir =5.5F;
	
	void Update () {
		if (TiempoParaDestruir <= 0){
			GameObject.Destroy(gameObject);
		}
	}
	void FixedUpdate () {
		TiempoParaDestruir -= Time.deltaTime;
	}
}
   